﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Lesson6
{
    class Program
    {
        static int lives;
        static int oppenedLettersCount;
        static string letterBuffer = " ";

        static void Main(string[] args)
        {
            string dictionaryPath = @"C:\Users\Paradise_Lost\Unity Projects\TMS\Projects\TMS_Lesson_6\Lesson6\word_rus.txt";
            string[] gameWords = File.ReadAllLines(dictionaryPath);
            Random randomWord = new Random();

            while (true)
            {
                // Выбираем случайное слово
                string word = gameWords[randomWord.Next(0, gameWords.Length)];
                char[] emptyWord = new char[word.Length];

                for (int i = 0; i < emptyWord.Length; i++)
                {
                    emptyWord[i] = '_';
                }

                Console.WriteLine($"В загаданном слове {word.Length} букв.");

                oppenedLettersCount = 0;
                lives = 5;

                while (IsAlive() && !IsAllLetersOppened(word))
                {
                    char letter;
                    bool isLetterFinded = false;

                    if (!LetterInput())
                    {
                        continue;
                    }
                    else
                    {
                        letter = letterBuffer[0];
                    }

                    for (int i = 0; i < word.Length; i++)
                    {
                        if (word[i] == letter && emptyWord[i] == '_')
                        {
                            emptyWord[i] = letter;
                            oppenedLettersCount++;
                            isLetterFinded = true;
                        }
                    }

                    if (isLetterFinded)
                    {
                        Console.WriteLine($"Буква угадана! Количество жизней: {lives}");
                    }
                    else
                    {
                        lives--;
                        Console.WriteLine($"Такой буквы нет! Количество жизней: {lives}");
                    }

                    Console.WriteLine(emptyWord);
                }

                GameResult(word);

                if (IsPlayAgain())
                    Console.Clear();
                else
                    break;
            }
        }

        public static bool IsAlive()
        {
            return lives > 0;
        }

        public static bool IsAllLetersOppened(string word)
        {
            return oppenedLettersCount == word.Length;
        }

        public static bool LetterInput()
        {
            Console.Write("Введите букву: ");
            letterBuffer = Console.ReadLine();

            if (letterBuffer.Length == 0 || letterBuffer.Length > 1 || !Char.IsLetter(letterBuffer[0]))
            {
                Console.WriteLine();
                Console.WriteLine("Нужно ввести одну букву! ");
                return false;
            }
            else
            {
                Console.Clear();
                return true;
            }
        }

        public static void GameResult(string word)
        {
            if (lives > 0)
            {
                Console.WriteLine("Слово угадано!");
                Console.WriteLine();
            }
            else
            {
                Console.WriteLine($"Слово \"{word}\" не отгадано! ");
                Console.WriteLine();
            }
        }

        public static bool IsPlayAgain()
        {
            Console.WriteLine("Сыграть еще раз? Y/N ");
            string answer = Console.ReadLine();
            if (answer == "y" || answer == "Y" || answer == "н" || answer == "Н")
                return true;
            else
                return false;
        }
    }
}
